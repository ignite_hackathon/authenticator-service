import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import User from '../../app/Models/User';

export default class UserSeeder extends BaseSeeder {
  public async run () {
    await User.createMany([
      {
        username: 'test',
        password: 'test',
        name: 'test',
        lastName: 'test'
      },
      {
        username: 'prueba',
        password: 'prueba',
        name: 'prueba',
        lastName: 'prueba'
      }
    ])
  }
}
