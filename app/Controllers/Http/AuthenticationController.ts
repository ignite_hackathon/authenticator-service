import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import AuthenticationService from '../../Services/AuthenticationService';
import User from '../../Models/User'

class AuthenticationController {
  public async getUsers({ request, response }: HttpContextContract) {
    return User.all();
  }
}

export default AuthenticationController;

