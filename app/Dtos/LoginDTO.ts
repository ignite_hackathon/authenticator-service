type LoginDTO = {
  password: string;
  username: string;
}
